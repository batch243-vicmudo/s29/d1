// [Section] Comparison Query Operators

// $gt/$gte operator
	/*
		-it allows us to find document that have field number value greater than or equal to a specified value.
		-Syntax:
		db.collectionName.find({field: {$gt:value}});
		db.collectonName.find({field: {$gte:value}});
	*/
	//$gt
	db.users.find({age: {$gt: 50}}).pretty();

	//$gte
	db.users.find({age: {$gte:21}}).pretty();

//$lt/lte operator
	/*
		-it allows us to find/retrieve documents that have field number values less than or equal to a specified value
		-Syntax:
		db.collectionName.find({field: {$lt:value}});
		db.collectonName.find({field: {$lte:value}});
	*/

	db.users.find({age: {$lt: 50}}).pretty();
	db.users.find({age: {$lte:76}}).pretty();

// $ne operator
/*
	-not equal operator, allows us to find documents that have field number values not equal to a specified value
	Syntax:
		db.users.find({field:{$ne:value}});
*/
	db.users.find({age: {$ne:68}});

// $in operator
/*
	-it allows us to find documents/document with specific match criteria one field using different values.
	Syntax:
		db.collectionName.find({field: {$in: [valueA, valueB]}});
*/

	db.users.find({lastName: {$in:["Doe", "Hawking"]}});

	db.users.find({courses: {$in:["HTML", "React"]}});

// [Section] logical Query Operators

//$or operator
/*
	-allows	us to find documents that match a single criteria from multiple provide search criteria.
	Syntax:
		db.collectionName.find($or : [{fieldA: valueA}, {fieldB: valueB}]);
*/
	db.users.find({$or: [{firstName: "Neil"}, {age: 25}]});

	db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 30}}]});

//$and operator
/*
	-allows us to find documents matching multiple provided criteria;
	Syntax:
	db.collectionName.find({$and : [{fieldA:valueA}, {fieldB:valueB}]})
*/
	db.users.find({$and: [{firstName: "Neil"}, {age: {$gt: 30}}]});

	db.users.find({$and: [{firstName: "Neil"}, {age: 25}]}); //false

// [Section] Field Projection
	/*
		-retrieving documents are common operations that we do and by default mongoDB query return the whole document as a response.
	*/
	//Inclusion
	/*
		-it allows us to include/ add specific fields only when retrieving documents.
		Syntax:
			db.users.find({criteria}, {fields: 1});
	*/

	db.users.find({firstName: "Jane"}, 
	{
		firstName: true,
		lastName: 1,
		age: 1
	}
	);

	db.users.find({$and: [{firstName: "Neil"}, {age: {$gt: 30}}]},
		{
			firstName: 1,
			lastName: 1,
			contact: 1
		});

	//Exclusion
	/*
		-allows us to exclude/remove specific fields only when retrieving documents.
		-The value provided is 0.
		Syntax:
		db.collectionName.find({criteria}, {fieldA:0})
	*/

	db.users.find({firstName: "Jane"},{
		contact: 0,
		department: 0
	});

	db.users.find({$and: [{firstName: "Neil"}, {age: {$gte : 25}}]},
	{
		age: false,
		courses : 0
		});	

	db.users.find({firstName: "Jane"}, {"contact.phone":1, _id:0});

	db.users.find({firstName: "Jane"}, {"contact.phone":0});
// di pwedeng pagsabayin inclusion and exclusion

// [Section] Evaluation query operators
/*
	-it allows us to find documents that match a specific string patter using regular expressions. (regex)
	Syntax:
		db.users.find({field: $regex: 'pattern', $options: '$optionValue'})
*/

	db.users.find({firstName: {$regex : "N"}});
	//case sensitive query
	db.users.find({firstName: {$regex : "N", $options: '$1'}});

	// CRUD operations

	db.users.UpdateOne({age: {$lte :17}},{
		$set:{
			firstName: "Chris",
			lastName: "Mortel"
		}
	})

	db.users.deleteOne({$and : [{firstName:"Chris"}, {lastName: "Mortel"}]});

	//db.users.find({ $and: [{ age: {$lte: 82} }, { age: {$gte: 76} }] });
	// 11/27/2022
	
	//db.getCollection('users').find({"contact.email": {$eq : "stephenhawking@gmail.com"}}, { firstName : 1, lastName : true});